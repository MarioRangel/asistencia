from rechumanos.views.buscar import Buscar
from rechumanos.views.permutas import permuta_editar
from rechumanos.views.checador import horario_filtro
from rechumanos.views.faltas import falta_filtro
from rechumanos.views.personales import personales_filtro
from rechumanos.views.viajes import viaje_filtro, viaje_nuevo
from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.views import LoginView, LogoutView


urlpatterns = [
    #Home
    path('', views.home, name='home'),

    #Empleados
    path('personal/', views.personales, name='personal'),
    path('personal_nuevo/', views.personal_nuevo, name='personal-nuevo'),
    path('personal/<int:pk>/', views.personal_editar, name='personal-editar'),
    path('personal/<int:pk>/eliminar/', views.personal_eliminar, name='personal-eliminar'),
    path('personal_filtro/', views.personales_filtro, name='personales_filtro'),

    #Viajes
    path('viajes/', views.viajes_all, name='viajes'),
    path('viaje_nuevo/', views.viaje_nuevo, name='viaje-nuevo'),
    path('viaje/<int:pk>/', views.viaje_editar, name='viaje-editar'),
    path('viaje/<int:pk>/eliminar/', views.viaje_eliminar, name='viaje-eliminar'),
    path('viajes_filtro/', views.viaje_filtro, name='viajes_filtro'),

    #Faltas
    path('faltas/', views.faltas_all, name='faltas'),
    path('falta_nueva/', views.falta_nueva, name='falta-nueva'),
    path('falta/<int:pk>/', views.falta_editar, name='falta-editar'),
    path('falta/<int:pk>/eliminar/', views.falta_eliminar, name='falta-eliminar'),
    path('faltas_filtro/', views.falta_filtro, name='faltas_filtro'),

    #CHECADOR
    path('checador/', views.checador_all, name='checador'),
    path('horario_nuevo/', views.horario_nuevo, name='horario-nuevo'),
    path('horario/<int:pk>/', views.horario_editar, name='horario-editar'),
    path('horario/<int:pk>/eliminar/', views.horario_eliminar, name='horario-eliminar'),
    path('horario_filtro/', views.horario_filtro, name='horario_filtro'),

    #PERMISOS
    path('permisos/', views.permisos_all, name='permisos'),
    path('permiso_nuevo/', views.permiso_nuevo, name='permiso-nuevo'),
    path('permiso/<int:pk>/', views.permiso_editar, name='permiso-editar'),
    path('permiso/<int:pk>/eliminar/', views.permiso_eliminar, name='permiso-eliminar'),
    path('permisos_filtro/', views.permiso_filtro, name='permisos_filtro'),

    #PERMUTAS
    path('permutas/', views.permutas_filtro, name='permutas'),
    path('permuta_nueva/', views.permuta_nueva, name='permuta-nueva'),
    path('permuta/<int:pk>/', views.permuta_editar, name='permuta-editar'),
    path('permuta/<int:pk>/eliminar/', views.permuta_eliminar, name='permuta-eliminar'),

    #FESTIVOS
    path('festivos/', views.festivos_filtro, name='festivos'),
    path('festivo_nuevo/', views.festivo_nuevo, name='festivo-nuevo'),
    path('festivo/<int:pk>/', views.festivo_editar, name='festivo-editar'),
    path('festivo/<int:pk>/eliminar/', views.festivo_eliminar, name='festivo-eliminar'),

    #DF
    path('df/', views.DF_all, name='DF_all'),
    path('df_nuevo/', views.df_nuevo, name='DF_nuevo'),
    path('df/<int:pk>/', views.df_editar, name='DF-editar'),
    path('df/<int:pk>/eliminar/', views.df_eliminar, name='DF-eliminar'),


    #VACACIONES
    path('vacaciones/', views.vacaciones_filtro, name='vacaciones'),
    path('vacaciones_nuevo/', views.vacaciones_nuevo, name='vacaciones-nuevo'),
    path('vacaciones/<int:pk>/', views.vacaciones_editar, name='vacaciones-editar'),
    path('vacaciones/<int:pk>/eliminar/', views.vacaciones_eliminar, name='vacaciones-eliminar'),

    #HORARIOMIXTO
    path('horariomixto/', views.mixto_filtro, name='mixto'),
    path('mixto_nuevo/', views.mixto_nuevo, name='mixto-nuevo'),
    path('horariomixto/<int:pk>/', views.mixto_editar, name='mixto-editar'),
    path('horariomixto/<int:pk>/eliminar/', views.mixto_eliminar, name='mixto-eliminar'),

    #AUTH
    path('login/', LoginView.as_view(template_name='rechumanos/login.html', redirect_authenticated_user=True), name='login'),
    path('logout/', LogoutView.as_view(template_name='rechumanos/logout.html'), name='logout'),
]
