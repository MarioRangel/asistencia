from django import forms
from django.forms import fields
from django.forms import widgets
from django.forms.forms import Form
from django.forms.models import ModelChoiceField, ModelForm
from django.forms.widgets import DateInput, TextInput
from .models import *
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Submit, HTML, Button, Row, Field
from crispy_forms.bootstrap import AppendedText, PrependedText, FormActions

class_form_select_sm = {'class':'form-control form-select-sm'}

SINO_CHOICES= [
    ('SI', 'SI'),
    ('NO', 'NO'),
    ]

TIPO_CHOICES= [
    ('CONFIANZA', 'CONFIANZA'),
    ('EVENTUAL', 'EVENTUAL'),
    ('ASAMILABLE', 'ASIMILABLE'),
]

FESTIVO_CHOICES= [
    ('DF', 'DIA FESTIVO'),
]

LUGAR_CHOICES= [
    ('E', 'ENSENADA'),
    ('TCT', 'TECATE'),
    ('T', 'TIJUANA'),
    ('ZC', 'ZONA COSTA'),
    ('O', 'OTRO')
]

TIPO_HORARIO_CHOICES= [
    ('MIXTO', 'MIXTO'),
    ('GUARDIA', 'GUARDIA'),
]


class DateInput(forms.DateInput):
    input_type = 'date'

class TimeInput(forms.TimeInput):
    input_type = 'text'   

class PersonalForm(forms.ModelForm):
    nivel = forms.ModelChoiceField(queryset=NivelPersonal.objects.all())
    idarea = forms.ModelChoiceField(queryset=Areas.objects.all())
    iddepartamento = forms.ModelChoiceField(queryset=Departamento.objects.all())
    idtipo_horario = forms.ModelChoiceField(queryset=horarios.objects.all())
    class Meta:
        model = personal
        fields = ['codigoempleado', 'nombre', 'paterno', 'materno','fecha_nac' , 'puesto', 'nivel', 'idarea', 'iddepartamento', 'checa', 'estatus', 'baja', 'fecha_baja', 'fecha_ingreso','idtipo_horario']
        labels = {
            'codigoempleado': 'No. de Empleado',
            'paterno': 'Apellido Paterno',
            'materno': 'Apellido Materno',
            'fecha_nac': 'Fecha de nacimiento',
            'idarea': 'Area',
            'iddepartamento': 'Departamento',
            'fecha_baja': 'Fecha de baja',
            'fecha_ingreso': 'Fecha de ingreso',
            'idtipo_horario': 'Tipo de horario',
        }

        widgets = {
            'codigoempleado': forms.NumberInput(attrs=class_form_select_sm),
            'nombre': forms.TextInput(attrs=class_form_select_sm),
            'paterno': forms.TextInput(attrs=class_form_select_sm),
            'materno': forms.TextInput(attrs=class_form_select_sm),
            'fecha_nac': DateInput(format=('%Y-%m-%d'), attrs={'class':'form-control-sm','type': 'date'}),
            'puesto': forms.TextInput(attrs=class_form_select_sm),
            'checa': forms.Select(choices=SINO_CHOICES),
            'estatus': forms.Select(choices=TIPO_CHOICES),
            'baja': forms.Select(choices=SINO_CHOICES),
            'fecha_baja': DateInput(format=('%Y-%m-%d'), attrs={'class':'form-control-sm','type': 'date'}),
            'fecha_ingreso': DateInput(format=('%Y-%m-%d'), attrs={'class':'form-control-sm','type': 'date'}),
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.helper = FormHelper(self)
            self.helper.layout = Layout(
                Div(
                    Field('codigoempleado', wrapper_class="col"),
                css_class='row'),
                Div(
                    Field('nombre', wrapper_class="col"),
                    Field('paterno', wrapper_class="col"),
                    Field('materno', wrapper_class="col"),
                css_class='row'),
                Div(
                    Field('fecha_nac', wrapper_class="col"),
                    Field('puesto', wrapper_class="col"),
                    Field('nivel', wrapper_class="col"),
                css_class='row'),
                Div(
                    Field('idarea', wrapper_class="col"),
                    Field('iddepartamento', wrapper_class="col"),
                    Field('checa', wrapper_class="col"),
                css_class='row'),
                 Div(
                    Field('estatus', wrapper_class="col"),
                    Field('baja', wrapper_class="col"),
                    Field('fecha_baja', wrapper_class="col"),
                css_class='row'),
                 Div(
                    Field('fecha_ingreso', wrapper_class="col"),
                    Field('idtipo_horario', wrapper_class="col"),
                css_class='row'),
            )

class ViajeForm(forms.ModelForm):
     personal = forms.ModelChoiceField(queryset=personal.objects.order_by('codigoempleado'))
     class Meta:
        model = Vi_vac_inc
        fields = ['personal', 'fecha', 'concepto', 'motivo', 'otros']
        labels = {
            'personal': 'Empleado',
            'concepto': 'Lugar'
        }

        widgets = {
            'fecha': DateInput(format=('%Y-%m-%d'), attrs={'class':'form-control-sm','type': 'date'}),
            'motivo': forms.TextInput(attrs=class_form_select_sm),
            'otros': forms.TextInput(attrs=class_form_select_sm),
            'concepto': forms.Select(choices=LUGAR_CHOICES),
        }

     def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.helper = FormHelper(self)
            self.helper.layout = Layout(
                Div(
                    Field('personal', wrapper_class="col"),
                css_class='row'),
                Div(
                    Field('fecha', wrapper_class="col"),
                    Field('concepto', wrapper_class="col"),
                css_class='row'),
                Div(
                    Field('otros', wrapper_class="col"),
                    Field('motivo', wrapper_class="col"),
                css_class='row'),
            )

class BuscarForm(forms.Form):
    codigo = forms.CharField(max_length=50, required=False, widget=forms.TextInput(attrs=class_form_select_sm))

    def  __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Div(
                Field('codigo', wrapper_class="col"),
            css_class='row'),

            FormActions(
                Submit('buscar', 'Buscar', style="background-color: #782484; border-color: #782484;"),
            )
        )


class FaltaForm(forms.ModelForm):
     personal = forms.ModelChoiceField(queryset=personal.objects.order_by('codigoempleado'))    
     class Meta:
        model = Faltas
        fields = ['personal', 'fecha', 'observaciones']
        labels = {
            'personal': 'Empleado'
        }

        widgets = {
            'fecha': DateInput(format=('%Y-%m-%d'), attrs={'class':'form-control-sm','type': 'date'}),
            'observaciones': forms.TextInput(attrs=class_form_select_sm),
        }

     def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.helper = FormHelper(self)
            self.helper.layout = Layout(
                Div(
                    Field('personal', wrapper_class="col"),
                css_class='row'),
                Div(
                    Field('fecha', wrapper_class="col"),
                css_class='row'),
                Div(
                    Field('observaciones', wrapper_class="col"),
                css_class='row'),
            )    


class ChecadorForm(forms.ModelForm):
    personal = forms.ModelChoiceField(queryset=personal.objects.order_by('codigoempleado'))
    class Meta:
        model = Checador
        fields = ['personal', 'fecha', 'hora_entrada', 'hora_salida', 'observaciones']
        labels = {
            'personal': 'Empleado',
            'hora_entrada': 'Hora de entrada',
            'hora_salida': 'Hora de salida'
        }

        widgets = {
            'fecha': DateInput(format=('%Y-%m-%d'), attrs={'class':'form-control-sm','type': 'date'}),
            'hora_entrada': TimeInput(format=('%H:%M'), attrs={'class':'form-control-sm','type': 'time'}),
            'hora_salida': TimeInput(format=('%H:%M'), attrs={'class':'form-control-sm','type': 'time'}),
            'observaciones': forms.TextInput(attrs=class_form_select_sm),
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.helper = FormHelper(self)
            self.helper.layout = Layout(
                Div(
                    Field('personal', wrapper_class="col"),
                css_class='row'),
                Div(
                    Field('fecha', wrapper_class="col"),
                css_class='row'),
                Div(
                    Field('hora_entrada', wrapper_class="col"),
                    Field('hora_salida', wrapper_class="col"),
                css_class='row'),
                Div(
                    Field('observaciones', wrapper_class="col"),
                css_class='row'),
            )    

class PermisosForm(forms.ModelForm):
    personal = forms.ModelChoiceField(queryset=personal.objects.order_by('codigoempleado'))
    class Meta:
        model = permisos
        fields = ['personal', 'fecha', 'tipo', 'hora', 'hora_regreso', 'observaciones']
        labels = {
            'personal': 'Empleado',
            'tipo': 'Tipo de permiso',
            'hora': 'Hora',
            'hora_regreso': 'Hora de regreso'
        }

        widgets = {
            'fecha': DateInput(format=('%Y-%m-%d'), attrs={'class':'form-control-sm','type': 'date'}),
            'tipo': forms.Select(attrs=class_form_select_sm),
            'hora': TimeInput(format=('%H:%M'), attrs={'class':'form-control-sm','type': 'time'}),
            'hora_regreso': TimeInput(format=('%H:%M'), attrs={'class':'form-control-sm','type': 'time'}),
            'observaciones': forms.TextInput(attrs=class_form_select_sm),
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.helper = FormHelper(self)
            self.helper.layout = Layout(
                Div(
                    Field('personal', wrapper_class="col"),
                css_class='row'),
                Div(
                    Field('fecha', wrapper_class="col"),
                    Field('tipo', wrapper_class="col"),
                css_class='row'),
                Div(
                    Field('hora', wrapper_class="col"),
                    Field('hora_regreso', wrapper_class="col"),
                css_class='row'),
                Div(
                    Field('observaciones', wrapper_class="col"),
                css_class='row'),
            )    

class filtroForm(forms.Form):
    codigoempleado = forms.CharField(max_length=100, required=False, widget=forms.TextInput(attrs=class_form_select_sm))    
    nombre = forms.CharField(max_length=100, required=False, widget=forms.TextInput(attrs=class_form_select_sm))

    def  __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Div(
                Field('nombre', wrapper_class="col"),
                Field('codigoempleado', wrapper_class="col"),
            css_class='row'),

            FormActions(
                Submit('filtrar', 'Filtrar', style="background-color: #782484; border-color: #782484;"),
            )
        )

class viajeFiltroForm(forms.Form):
    codigoempleado = forms.CharField(max_length=100, required=False, widget=forms.TextInput(attrs=class_form_select_sm))

    def  __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Div(
                Field('codigoempleado', wrapper_class="col"),

            FormActions(
                Submit('filtrar', 'Filtrar', style="background-color: #782484; border-color: #782484;"),
            ))
        )


class FaltaFiltroForm(forms.Form):
    codigoempleado = forms.CharField(max_length=100, required=False, widget=forms.TextInput(attrs=class_form_select_sm))

    def  __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Div(
                Field('codigoempleado', wrapper_class="col"),

            FormActions(
                Submit('filtrar', 'Filtrar', style="background-color: #782484; border-color: #782484;"),
            ))
        )

class HorarioFiltroForm(forms.Form):
    codigoempleado = forms.CharField(max_length=100, required=False, widget=forms.TextInput(attrs=class_form_select_sm))

    def  __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Div(
                Field('codigoempleado', wrapper_class="col"),

            FormActions(
                Submit('filtrar', 'Filtrar', style="background-color: #782484; border-color: #782484;"),
            ))
        )

class PermisosFiltroForm(forms.Form):
    codigoempleado = forms.CharField(max_length=100, required=False, widget=forms.TextInput(attrs=class_form_select_sm))

    def  __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Div(
                Field('codigoempleado', wrapper_class="col"),

            FormActions(
                Submit('filtrar', 'Filtrar', style="background-color: #782484; border-color: #782484;"),
            ))
        )

class PermutasFiltroForm(forms.Form):
    codigoempleado = forms.CharField(max_length=100, required=False, widget=forms.TextInput(attrs=class_form_select_sm))

    def  __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Div(
                Field('codigoempleado', wrapper_class="col"),

            FormActions(
                Submit('filtrar', 'Filtrar', style="background-color: #782484; border-color: #782484;"),
            ))
        )

class PermutaForm(forms.ModelForm):
    personal = forms.ModelChoiceField(queryset=personal.objects.order_by('codigoempleado'))
    class Meta:
        model = Permuta
        fields = ['personal', 'fecha', 'hora_entrada', 'hora_salida', 'otro', 'fecha_descanso', 'observaciones']
        labels = {
            'personal': 'Empleado',
            'hora_entrada': 'Hora de entrada',
            'hora_salida': 'Hora de salida',
            'fecha_descanso': 'Fecha de descanso'
        }

        widgets = {
            'fecha': DateInput(format=('%Y-%m-%d'), attrs={'class':'form-control-sm','type': 'date'}),
            'hora_entrada': TimeInput(format=('%H:%M'), attrs={'class':'form-control-sm','type': 'time'}),
            'hora_salida': TimeInput(format=('%H:%M'), attrs={'class':'form-control-sm','type': 'time'}),
            'otro': forms.TextInput(attrs=class_form_select_sm),
            'fecha_descanso': DateInput(format=('%Y-%m-%d'), attrs={'class':'form-control-sm','type': 'date'}),
            'observaciones': forms.TextInput(attrs=class_form_select_sm),
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.helper = FormHelper(self)
            self.helper.layout = Layout(
                Div(
                    Field('personal', wrapper_class="col"),
                css_class='row'),
                Div(
                    Field('fecha', wrapper_class="col"),
                    Field('fecha_descanso', wrapper_class="col"),
                css_class='row'),
                Div(
                    Field('hora_entrada', wrapper_class="col"),
                    Field('hora_salida', wrapper_class="col"),
                css_class='row'),
                Div(
                    Field('otro', wrapper_class="col"),
                    Field('observaciones', wrapper_class="col"),
                css_class='row'),
            )    

class FestivosFiltroForm(forms.Form):
    codigoempleado = forms.CharField(max_length=100, required=False, widget=forms.TextInput(attrs=class_form_select_sm))

    def  __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Div(
                Field('codigoempleado', wrapper_class="col"),

            FormActions(
                Submit('filtrar', 'Filtrar', style="background-color: #782484; border-color: #782484;"),
            ))
        )

class FestivoForm(forms.ModelForm):
    personal = forms.ModelChoiceField(queryset=personal.objects.order_by('codigoempleado'))
    class Meta:
        model = Vi_vac_inc
        fields = ['personal', 'fecha', 'concepto', 'motivo', 'otros']
        labels = {
            'personal': 'Empleado',
        }

        widgets = {
            'fecha': DateInput(format=('%Y-%m-%d'), attrs={'class':'form-control-sm','type': 'date'}),
            'motivo': forms.TextInput(attrs=class_form_select_sm),
            'otros': forms.TextInput(attrs=class_form_select_sm),
            'concepto': forms.Select(choices=FESTIVO_CHOICES),
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.helper = FormHelper(self)
            self.helper.layout = Layout(
                Div(
                    Field('personal', wrapper_class="col"),
                css_class='row'),
                Div(
                    Field('fecha', wrapper_class="col"),
                    Field('concepto', wrapper_class="col"),
                css_class='row'),
                Div(
                    Field('motivo', wrapper_class="col"),
                    Field('otros', wrapper_class="col"),
                css_class='row'),
            )

class VacacionesFiltroForm(forms.Form):
    codigoempleado = forms.CharField(max_length=100, required=False, widget=forms.TextInput(attrs=class_form_select_sm))

    def  __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Div(
                Field('codigoempleado', wrapper_class="col"),

            FormActions(
                Submit('filtrar', 'Filtrar', style="background-color: #782484; border-color: #782484;"),
            ))
        )

class VacacionesForm(forms.ModelForm):
    personal = forms.ModelChoiceField(queryset=personal.objects.order_by('codigoempleado'))
    class Meta:
        model = Vacaciones
        fields = ['personal', 'fecha_inicio', 'fecha_termino', 'dias', 'observaciones']
        labels = {
            'personal': 'Empleado',
            'fecha_inicio': 'Fecha de inicio',
            'fecha_termino': 'Fecha Terminacion'
        }

        widgets = {
            'fecha_inicio': DateInput(format=('%Y-%m-%d'), attrs={'class':'form-control-sm','type': 'date'}),
            'fecha_termino': DateInput(format=('%Y-%m-%d'), attrs={'class':'form-control-sm','type': 'date'}),
            'dias': forms.NumberInput(),
            'observaciones': forms.TextInput(attrs=class_form_select_sm),
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.helper = FormHelper(self)
            self.helper.layout = Layout(
                Div(
                    Field('personal', wrapper_class="col"),
                css_class='row'),
                Div(
                    Field('fecha_inicio', wrapper_class="col"),
                    Field('fecha_termino', wrapper_class="col"),
                css_class='row'),
                Div(
                    Field('dias', wrapper_class="col"),
                    Field('observaciones', wrapper_class="col"),
                css_class='row'),
            )

class DFForm(forms.ModelForm):
    class Meta:
        model = Festivos
        fields = ['fecha', 'descripcion']

        widgets = {
            'fecha': DateInput(format=('%Y-%m-%d'), attrs={'class':'form-control-sm','type': 'date'}),
            'descripcion': forms.TextInput(attrs=class_form_select_sm),
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.helper = FormHelper(self)
            self.helper.layout = Layout(
                Div(
                    Field('fecha', wrapper_class="col"),
                css_class='row'),
                Div(
                    Field('descripcion', wrapper_class="col"),
                css_class='row'),
            )                           

class CodigoempleadoFiltroForm(forms.Form):
    codigoempleado = forms.CharField(max_length=100, required=False, widget=forms.TextInput(attrs=class_form_select_sm))

    def  __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Div(
                Field('codigoempleado', wrapper_class="col"),

            FormActions(
                Submit('filtrar', 'Filtrar', style="background-color: #782484; border-color: #782484;"),
            ))
        )

class MixtoForm(forms.ModelForm):
    personal = forms.ModelChoiceField(queryset=personal.objects.order_by('codigoempleado'))
    fecha_termina = forms.DateTimeField(widget=forms.DateTimeInput(format=('%Y-%m-%d'), attrs={'class':'form-control-sm','type': 'date'}))
    class Meta:
        model = HorarioMixto
        fields = ['personal','hora_inicio', 'hora_termina','fecha','tipo','observaciones']

        widgets = {
            'hora_inicio': TimeInput(format=('%H:%M'), attrs={'class':'form-control-sm','type': 'time'}),
            'hora_termina': TimeInput(format=('%H:%M'), attrs={'class':'form-control-sm','type': 'time'}),
            'fecha': DateInput(format=('%Y-%m-%d'), attrs={'class':'form-control-sm','type': 'date'}),
            'tipo': forms.Select(choices=TIPO_HORARIO_CHOICES),
            'observaciones': forms.TextInput(attrs=class_form_select_sm),
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.helper = FormHelper(self)
            self.helper.layout = Layout(
                Div(
                    Field('personal', wrapper_class="col"),
                css_class='row'),
                Div(
                    Field('tipo', wrapper_class="col"),
                    Field('fecha', wrapper_class="col"),
                    Field('fecha_termina', wrapper_class="col"),
                css_class='row'),
                Div(
                    Field('hora_inicio', wrapper_class="col"),
                    Field('hora_termina', wrapper_class="col"),
                css_class='row'),
                Div(
                    Field('observaciones', wrapper_class="col"),
                css_class='row'),
            )

class MixtoBForm(forms.Form):
    personal = forms.ModelChoiceField(queryset=personal.objects.order_by('codigoempleado'))
    fecha_termina = forms.DateInput(format=('%Y-%m-%d'), attrs={'class':'form-control-sm','type': 'date'})
    hora_inicio =  forms.TimeInput(format=('%H:%M'), attrs={'class':'form-control-sm','type': 'time'})
    hora_termina = forms.TimeInput(format=('%H:%M'), attrs={'class':'form-control-sm','type': 'time'})
    fecha = forms.DateInput(format=('%Y-%m-%d'), attrs={'class':'form-control-sm','type': 'date'})
    tipo = forms.Select(choices=TIPO_HORARIO_CHOICES)
    observacion = forms.TextInput(attrs=class_form_select_sm)

    fields = ['personal','hora_inicio', 'hora_termina','fecha','tipo','observaciones','fecha_termina']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Div(
                Field('personal', wrapper_class="col"),
            css_class='row'),
             Div(
                Field('tipo', wrapper_class="col"),
                Field('fecha', wrapper_class="col"),
                Field('fecha_termina', wrapper_class="col"),
            css_class='row'),
            Div(
                Field('hora_inicio', wrapper_class="col"),
                Field('hora_termina', wrapper_class="col"),
            css_class='row'),
            Div(
                Field('observacion', wrapper_class="col"),
            css_class='row'),
        )




