$(document).ready(function(){
    $( ".nuevo-modal" ).click(function() {
        var endpoint = $(this).attr("data-url");
        $.ajax({
            url: endpoint,
            type: "GET",
            success(response) {
                $("#modaldiv").html(response);
                $('#nuevoModal').modal('show')
            },
        });   
    });

    $( ".editar-modal" ).click(function() {
        var endpoint = $(this).attr("data-url");
        $.ajax({
            url: endpoint,
            type: "GET",
            success(response) {
                $("#modaldiv").html(response);
                $('#editarModal').modal('show')
            },
        });   
    });

    $( ".eliminar-modal" ).click(function() {
        var endpoint = $(this).attr("data-url");
        $.ajax({
            url: endpoint,
            type: "GET",
            success(response) {
                $("#modaldiv").html(response);
                $('#eliminarModal').modal('show')
            },
        });   
    });

    feather.replace()
});

