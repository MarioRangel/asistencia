# Generated by Django 3.2.5 on 2021-09-29 21:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('rechumanos', '0009_auto_20210927_0855'),
    ]

    operations = [
        migrations.CreateModel(
            name='HorarioMixto',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('hora_inicio', models.TimeField()),
                ('hora_termina', models.TimeField()),
                ('fecha_inicia', models.DateTimeField()),
                ('fecha_termina', models.DateTimeField()),
                ('observaciones', models.CharField(blank=True, max_length=255, null=True)),
                ('personal', models.ForeignKey(db_column='codigoempleado', on_delete=django.db.models.deletion.DO_NOTHING, to='rechumanos.personal')),
            ],
        ),
    ]
