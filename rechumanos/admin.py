from django.contrib import admin

from .models import *

admin.site.register(Areas)
admin.site.register(Departamento)
admin.site.register(NivelPersonal)
admin.site.register(horarios)
admin.site.register(personal)
admin.site.register(lugar)
admin.site.register(viajes)
admin.site.register(Faltas)
admin.site.register(Checador)
admin.site.register(tipo_permisos)
admin.site.register(permisos)


# Register your models here.
