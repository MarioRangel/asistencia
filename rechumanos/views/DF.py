from django.forms.forms import Form
from django.shortcuts import get_object_or_404, render, redirect
from ..models import *
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse, Http404
from django.core import serializers
from rechumanos.forms import DFForm
from django.db.models.query_utils import Q

@login_required
def DF_all(request):
    if request.user.is_authenticated:
        Viajes = Festivos.objects.all().order_by('fecha')
        context = { 'festivos': Viajes, 'url_name': 'DF_all'}
        return render(request, 'rechumanos/df/content.html', context)
    else:
        return redirect('login')

@login_required
def df_nuevo(request):
     if request.method == 'POST':
         current_user = get_object_or_404(User, pk=request.user.pk)
         form = DFForm(request.POST)
         if form.is_valid():
            festivo = form.save(commit=False)
            festivo.user = current_user
            festivo.orden = 1
            festivo.save()
            messages.success(request, 'Registro exitoso!')
            return redirect('DF_all')
     else:
        form =  DFForm()
     return render(request, 'rechumanos/df/modal-nuevo.html', {'form': form})

@login_required
def df_editar(request,pk):
    festivos = get_object_or_404(Festivos,pk = pk)
    if request.method == 'POST':
        form = DFForm(request.POST, instance= festivos)
        if form.is_valid():
            form.save()
            messages.success(request, 'Edicion exitosa!')
            return redirect('DF_all')
    else:
        form = DFForm(instance= festivos)      
    return render(request, 'rechumanos/df/modal-editar.html', {'form': form, 'Festivos':  festivos})

@login_required
def df_eliminar(request, pk):
    festivos = get_object_or_404(Festivos,pk = pk)       
    if request.is_ajax():
        context = { 'Festivos': festivos}
        return render(request, 'rechumanos/df/modal-eliminar.html', context)
    else:
        festivos.delete()
        messages.success(request, 'Eliminación exitosa!')
        return redirect('DF_all')         