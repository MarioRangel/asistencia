
from django.shortcuts import get_object_or_404, render, redirect
from ..models import *
#from ..forms import *
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse, Http404
from django.core import serializers

# Create your views here.
@login_required
def home(request):
    if request.user.is_authenticated:
        context = { 'url_name': 'home'}
        return render(request, 'rechumanos/menu/home.html', context)
    else:
        return redirect('login')