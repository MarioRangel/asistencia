from django.forms.forms import Form
from django.shortcuts import get_object_or_404, render, redirect
from ..models import *
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse, Http404
from django.core import serializers
from rechumanos.forms import FestivoForm,FestivosFiltroForm
from django.db.models.query_utils import Q

@login_required
def festivos_filtro(request):
    codigoempleado = ""
    if request.method == 'POST':
         form = FestivosFiltroForm(request.POST)
         if form.is_valid():
             codigoempleado = form.cleaned_data['codigoempleado']
    else:
        form = FestivosFiltroForm()

    festivos = Filtrar_festivos(codigoempleado)
    context = {'form': form, 'festivos': festivos, 'url_name': 'festivos_filtro'}
    return render(request, 'rechumanos/festivos/filtro.html', context)  

@login_required
def festivo_nuevo(request):
     if request.method == 'POST':
         current_user = get_object_or_404(User, pk=request.user.pk)
         form = FestivoForm(request.POST)
         if form.is_valid():
            festivo = form.save(commit=False)
            festivo.user = current_user
            festivo.orden = 1
            festivo.save()
            messages.success(request, 'Registro exitoso!')
            return redirect('festivos')
     else:
        form = FestivoForm()
     return render(request, 'rechumanos/festivos/modal-nuevo.html', {'form': form}) 
     
@login_required
def festivo_editar(request,pk):
    festivos = get_object_or_404(Vi_vac_inc,pk = pk)
    if request.method == 'POST':
        form = FestivoForm(request.POST, instance= festivos)
        if form.is_valid():
            form.save()
            messages.success(request, 'Edicion exitosa!')
            return redirect('festivos')
    else:
        form = FestivoForm(instance= festivos)      
    return render(request, 'rechumanos/festivos/modal-editar.html', {'form': form, 'Vi_vac_inc':  festivos})

@login_required
def festivo_eliminar(request, pk):
    festivos = get_object_or_404(Vi_vac_inc,pk = pk)       
    if request.is_ajax():
        context = { 'Vi_vac_inc': festivos}
        return render(request, 'rechumanos/festivos/modal-eliminar.html', context)
    else:
        festivos.delete()
        messages.success(request, 'Eliminación exitosa!')
        return redirect('festivos')            

def Filtrar_festivos(codigoempleado):
    if codigoempleado != '':
        festivos = Vi_vac_inc.objects.filter(Q(personal__codigoempleado__icontains = codigoempleado) & Q(concepto__icontains = 'DF'))
    else:
        festivos = Vi_vac_inc.objects.filter(Q(concepto__icontains = 'DF')).order_by('-id')[:50]
    
    return festivos   