from django.forms.forms import Form
from django.shortcuts import get_object_or_404, render, redirect
from ..models import *
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse, Http404
from django.core import serializers
from rechumanos.forms import PermisosForm,PermisosFiltroForm
from django.db.models.query_utils import Q

# Create your views here.
@login_required
def permisos_all(request):
    if request.user.is_authenticated:
        permiso = permisos.objects.all()
        context = { 'permisos': permiso, 'url_name': 'permisos'}
        return render(request, 'rechumanos/permisos/content.html', context)
    else:
        return redirect('login')

@login_required
def permiso_nuevo(request):
     if request.method == 'POST':
         current_user = get_object_or_404(User, pk=request.user.pk)
         form = PermisosForm(request.POST)
         if form.is_valid():
            permiso = form.save(commit=False)
            permiso.user = current_user
            permiso.orden = 1
            permiso.save()
            messages.success(request, 'Registro exitoso!')
            return redirect('permisos_filtro')
     else:
        form = PermisosForm()
     return render(request, 'rechumanos/permisos/modal-nuevo.html', {'form': form})        

@login_required
def permiso_editar(request,pk):
    permiso = get_object_or_404(permisos,pk = pk)
    if request.method == 'POST':
        form = PermisosForm(request.POST, instance= permiso)
        if form.is_valid():
            form.save()
            messages.success(request, 'Edicion exitosa!')
            return redirect('permisos_filtro')
    else:
        form = PermisosForm(instance= permiso)      
    return render(request, 'rechumanos/permisos/modal-editar.html', {'form': form, 'permisos':  permiso})   

@login_required
def permiso_eliminar(request, pk):
    permiso = get_object_or_404(permisos,pk = pk)       
    if request.is_ajax():
        context = { 'permisos': permiso}
        return render(request, 'rechumanos/permisos/modal-eliminar.html', context)
    else:
        permiso.delete()
        messages.success(request, 'Eliminación exitosa!')
        return redirect('permisos_filtro')   

@login_required
def permiso_filtro(request):
    codigoempleado = ""
    if request.method == 'POST':
         form = PermisosFiltroForm(request.POST)
         if form.is_valid():
             codigoempleado = form.cleaned_data['codigoempleado']
    else:
        form = PermisosFiltroForm()

    permisos = Filtrar_permisos(codigoempleado)
    context = {'form': form, 'permisos': permisos, 'url_name': 'permisos_filtro'}
    return render(request, 'rechumanos/permisos/filtro.html', context)                       


def Filtrar_permisos(codigoempleado):
    if codigoempleado != '':
        permiso = permisos.objects.filter(Q(personal__codigoempleado__icontains = codigoempleado))
    else:
        permiso = permisos.objects.all().order_by('-id')
    
    return permiso      