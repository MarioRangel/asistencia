from django.db.models.query_utils import Q
from rechumanos.forms import PersonalForm, filtroForm
from django.forms.forms import Form
from django.shortcuts import get_object_or_404, render, redirect
from ..models import *
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse, Http404
from django.core import serializers

# Create your views here.
@login_required
def personales(request):    
    if request.user.is_authenticated:
        personales = personal.objects.all()
        context = { 'personales': personales, 'url_name': 'personales'}
        return render(request, 'rechumanos/personal/content.html', context)
    else:
        return redirect('login')

@login_required
def personal_nuevo(request):
    if request.method == 'POST':
         current_user = get_object_or_404(User, pk=request.user.pk)
         form = PersonalForm(request.POST)
         if form.is_valid():
             personal = form.save(commit=False)
             personal.user = current_user
             personal.orden = 1
             personal.save()
             messages.success(request, 'Registro exitoso!')
             return redirect('personales_filtro')
    else:
        form = PersonalForm()
    return render(request, 'rechumanos/personal/modal-nuevo.html', {'form': form})             

@login_required
def personal_editar(request, pk):
    Personal = get_object_or_404(personal, pk = pk)
    if request.method == 'POST':
        form = PersonalForm(request.POST, instance=Personal)
        if form.is_valid():
            form.save()
            messages.success(request, 'Edicion de empleado exitosa!')
            return redirect('personales_filtro')
    else:
        form = PersonalForm(instance=Personal)
    return render(request, 'rechumanos/personal/modal-editar.html', {'form': form, 'personal': Personal})        

@login_required
def personal_eliminar(request, pk):
    Personal = get_object_or_404(personal,pk = pk)
    if request.is_ajax():
        context = { 'personal': Personal}
        return render(request, 'rechumanos/personal/modal-eliminar.html', context)
    else:
        Personal.delete()
        messages.success(request, 'Eliminación de empleado exitosa!')
        return redirect('personales_filtro')   

@login_required
def personales_filtro(request):    
    nombre = ""
    codigoempleado = ""
    if request.method == 'POST':
        form = filtroForm(request.POST)
        if form.is_valid():
            nombre = form.cleaned_data['nombre']
            codigoempleado = form.cleaned_data['codigoempleado']
        
    else:
         form = filtroForm()

    personales = Filtrar_personal(codigoempleado,nombre)
    context = {'form': form, 'personales': personales, 'url_name': 'personales_filtro'}
    return render(request, 'rechumanos/personal/filtro.html', context)        

def Filtrar_personal(codigoempleado,nombre):
    if codigoempleado != '':
        personales =  personal.objects.filter(Q(codigoempleado__icontains = codigoempleado))
    if nombre != '':
        personales = personal.objects.filter(Q(nombre__icontains = nombre) | Q(paterno__icontains = nombre) | Q(materno__icontains = nombre))
    if codigoempleado == '' and nombre == '':
        personales = personal.objects.all().order_by('codigoempleado')[:50]

    return personales
        