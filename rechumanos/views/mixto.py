from django.forms.fields import DateTimeField
from django.forms.forms import Form
from django.shortcuts import get_object_or_404, render, redirect
from ..models import *
from datetime import date, time, datetime,timedelta
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse, Http404
from django.core import serializers
from rechumanos.forms import CodigoempleadoFiltroForm,MixtoForm,MixtoBForm
from django.db.models.query_utils import Q

# Create your views here.
@login_required
def mixto_filtro(request):
    codigoempleado = ""
    if request.method == 'POST':
        form = CodigoempleadoFiltroForm(request.POST)
        if form.is_valid():
            codigoempleado = form.cleaned_data['codigoempleado']
    else:
        form = CodigoempleadoFiltroForm()

    horario = Filtrar_horarios(codigoempleado)
    context = {'form': form, 'mixto': horario, 'url_name': 'mixto'}
    return render(request, 'rechumanos/mixto/filtro.html', context)

def Filtrar_horarios(codigoempleado):
    if codigoempleado != '':
        horario = HorarioMixto.objects.filter(Q(personal__codigoempleado__icontains = codigoempleado)).order_by('-fecha')
    else:
        horario = HorarioMixto.objects.all().order_by('-fecha')

    return horario    

@login_required
def mixto_nuevo(request):
     if request.method == 'POST':
         current_user = get_object_or_404(User, pk=request.user.pk)
         form = MixtoForm(request.POST)
         if form.is_valid(): 
            mixto = form.save(commit=False)
            fecha_inicio = form.cleaned_data['fecha']
            fecha_termina = form.cleaned_data['fecha_termina']
            delta = timedelta(days=1)
            number = 0
            while fecha_inicio <= fecha_termina:
                if number == 0:
                    mixto.fecha = fecha_inicio
                    mixto.user = current_user
                    mixto.orden = 1
                    mixto.save()
                else:
                    horariomix = HorarioMixto()
                    horariomix.personal = form.cleaned_data['personal']   
                    horariomix.tipo =  form.cleaned_data['tipo']  
                    horariomix.fecha = fecha_inicio
                    horariomix.hora_inicio = form.cleaned_data['hora_inicio']
                    horariomix.hora_termina = form.cleaned_data['hora_termina']
                    horariomix.observaciones = form.cleaned_data['observaciones']
                    horariomix.save()

                number = number + 1    
                fecha_inicio += delta
            
            messages.success(request, 'Registro exitoso!')
            return redirect('mixto')
     else:
        form = MixtoForm()
     return render(request, 'rechumanos/mixto/modal-nuevo.html', {'form': form})

@login_required
def mixto_editar(request,pk):
    mixto = get_object_or_404(HorarioMixto,pk = pk)
    if request.method == 'POST':
        form = MixtoForm(request.POST, instance= mixto)
        if form.is_valid():
            form.save()
            messages.success(request, 'Edicion exitosa!')
            return redirect('mixto')
    else:
        form = MixtoForm(instance= mixto)      
    return render(request, 'rechumanos/mixto/modal-editar.html', {'form': form, 'HorarioMixto':  mixto})

@login_required
def mixto_eliminar(request, pk):
    mixto = get_object_or_404(HorarioMixto,pk = pk)       
    if request.is_ajax():
        context = { 'HorarioMixto': mixto}
        return render(request, 'rechumanos/mixto/modal-eliminar.html', context)
    else:
        mixto.delete()
        messages.success(request, 'Eliminación exitosa!')
        return redirect('mixto')