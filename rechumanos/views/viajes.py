from rechumanos.forms import  ViajeForm,viajeFiltroForm
from django.forms.forms import Form
from django.shortcuts import get_object_or_404, render, redirect
from ..models import *
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse, Http404
from django.core import serializers
from django.db.models.query_utils import Q

# Create your views here.
@login_required
def viajes_all(request):
    if request.user.is_authenticated:
        Viajes = viajes.objects.all()
        context = { 'viajes': Viajes, 'url_name': 'viajes'}
        return render(request, 'rechumanos/viajes/content.html', context)
    else:
        return redirect('login')


@login_required
def viaje_nuevo(request):
     if request.method == 'POST':
         current_user = get_object_or_404(User, pk=request.user.pk)
         form = ViajeForm(request.POST)
         if form.is_valid():
            Viajes = form.save(commit=False)
            Viajes.user = current_user
            Viajes.orden = 1
            Viajes.save()
            messages.success(request, 'Registro exitoso!')
            return redirect('viajes_filtro')
     else:
        form = ViajeForm()
     return render(request, 'rechumanos/viajes/modal-nuevo.html', {'form': form})

@login_required
def viaje_editar(request,pk):
    Viaje = get_object_or_404(Vi_vac_inc,pk = pk)
    if request.method == 'POST':
        form = ViajeForm(request.POST, instance=Viaje)
        if form.is_valid():
            form.save()
            messages.success(request, 'Edicion de viaje exitosa!')
            return redirect('viajes_filtro')
    else:
        form = ViajeForm(instance=Viaje)      
    return render(request, 'rechumanos/viajes/modal-editar.html', {'form': form, 'viajes': Viaje})   

@login_required
def viaje_eliminar(request, pk):
    Viaje = get_object_or_404(Vi_vac_inc,pk = pk)       
    if request.is_ajax():
        context = { 'viajes': Viaje}
        return render(request, 'rechumanos/viajes/modal-eliminar.html', context)
    else:
        Viaje.delete()
        messages.success(request, 'Eliminación de viaje exitosa!')
        return redirect('viajes_filtro') 

@login_required
def viaje_filtro(request):
    codigoempleado = ""
    if request.method == 'POST':
        form = viajeFiltroForm(request.POST)
        if form.is_valid():
            codigoempleado = form.cleaned_data['codigoempleado']
    else:
        form = viajeFiltroForm()

    viaje = Filtrar_viajes(codigoempleado)
    context = {'form': form, 'viajes': viaje, 'url_name': 'viajes_filtro'}
    return render(request, 'rechumanos/viajes/filtro.html', context)                  


def Filtrar_viajes(codigoempleado):
    if codigoempleado != '':
        viaje = Vi_vac_inc.objects.filter(Q(personal__codigoempleado__icontains = codigoempleado) & (Q(concepto__icontains = 'E') | Q(concepto__icontains = 'T') | Q(concepto__icontains = 'TCT') | Q(concepto__icontains = 'ZC') | Q(concepto__icontains = 'O')))
    else:
        viaje = Vi_vac_inc.objects.filter(Q(concepto__icontains = 'E') | Q(concepto__icontains = 'T') | Q(concepto__icontains = 'TCT') | Q(concepto__icontains = 'ZC') | Q(concepto__icontains = 'O')).order_by('-id')[:50]
    
    return viaje            

