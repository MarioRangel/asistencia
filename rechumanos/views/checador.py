from django.forms.forms import Form
from django.shortcuts import get_object_or_404, render, redirect
from ..models import *
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse, Http404
from django.core import serializers
from rechumanos.forms import ChecadorForm,HorarioFiltroForm
from django.db.models.query_utils import Q

# Create your views here.
@login_required
def checador_all(request):
    if request.user.is_authenticated:
        checador = Checador.objects.all()
        context = { 'checador': checador, 'url_name': 'checador'}
        return render(request, 'rechumanos/checador/content.html', context)
    else:
        return redirect('login')

@login_required
def horario_nuevo(request):
     if request.method == 'POST':
         current_user = get_object_or_404(User, pk=request.user.pk)
         form = ChecadorForm(request.POST)
         if form.is_valid():
            horario = form.save(commit=False)
            horario.user = current_user
            horario.orden = 1
            horario.save()
            messages.success(request, 'Registro exitoso!')
            return redirect('horario_filtro')
     else:
        form = ChecadorForm()
     return render(request, 'rechumanos/checador/modal-nuevo.html', {'form': form})      

@login_required
def horario_editar(request,pk):
    horario = get_object_or_404(Checador,pk = pk)
    if request.method == 'POST':
        form = ChecadorForm(request.POST, instance= horario)
        if form.is_valid():
            form.save()
            messages.success(request, 'Edicion exitosa!')
            return redirect('horario_filtro')
    else:
        form = ChecadorForm(instance= horario)      
    return render(request, 'rechumanos/checador/modal-editar.html', {'form': form, 'Checador':  horario})

@login_required
def horario_eliminar(request, pk):
    horario = get_object_or_404(Checador,pk = pk)       
    if request.is_ajax():
        context = { 'Checador': horario}
        return render(request, 'rechumanos/checador/modal-eliminar.html', context)
    else:
        horario.delete()
        messages.success(request, 'Eliminación exitosa!')
        return redirect('horario_filtro')  

@login_required
def horario_filtro(request):
    codigoempleado = ""
    if request.method == 'POST':
        form = HorarioFiltroForm(request.POST)
        if form.is_valid():
            codigoempleado = form.cleaned_data['codigoempleado']
    else:
        form = HorarioFiltroForm()

    horario = Filtrar_horarios(codigoempleado)
    context = {'form': form, 'checador': horario, 'url_name': 'horario_filtro'}
    return render(request, 'rechumanos/checador/filtro.html', context)            



def Filtrar_horarios(codigoempleado):
    if codigoempleado != '':
        horario = Checador.objects.filter(Q(personal__codigoempleado__icontains = codigoempleado))
    else:
        horario = Checador.objects.all().order_by('-id')

    return horario                   