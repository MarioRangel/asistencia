from django.forms.forms import Form
from django.shortcuts import get_object_or_404, render, redirect
from ..models import *
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse, Http404
from django.core import serializers
from rechumanos.forms import PermutaForm, PermutasFiltroForm
from django.db.models.query_utils import Q

@login_required
def permutas_filtro(request):
    codigoempleado = ""
    if request.method == 'POST':
         form =  PermutasFiltroForm(request.POST)
         if form.is_valid():
             codigoempleado = form.cleaned_data['codigoempleado']
    else:
        form =  PermutasFiltroForm()

    permutas = Filtrar_permutas(codigoempleado)
    context = {'form': form, 'permutas': permutas, 'url_name': 'permutas_filtro'}
    return render(request, 'rechumanos/permutas/filtro.html', context)

@login_required
def permuta_nueva(request):
     if request.method == 'POST':
         current_user = get_object_or_404(User, pk=request.user.pk)
         form = PermutaForm(request.POST)
         if form.is_valid():
            permutas = form.save(commit=False)
            permutas.user = current_user
            permutas.orden = 1
            permutas.save()
            messages.success(request, 'Registro exitoso!')
            return redirect('permutas')
     else:
        form = PermutaForm()
     return render(request, 'rechumanos/permutas/modal-nuevo.html', {'form': form})   

@login_required
def permuta_editar(request,pk):
    permutas = get_object_or_404(Permuta,pk = pk)
    if request.method == 'POST':
        form = PermutaForm(request.POST, instance= permutas)
        if form.is_valid():
            form.save()
            messages.success(request, 'Edicion exitosa!')
            return redirect('permutas')
    else:
        form = PermutaForm(instance= permutas)      
    return render(request, 'rechumanos/permutas/modal-editar.html', {'form': form, 'permuta': permutas})   

@login_required
def permuta_eliminar(request, pk):
    permutas = get_object_or_404(Permuta,pk = pk)       
    if request.is_ajax():
        context = { 'permuta': permutas}
        return render(request, 'rechumanos/permutas/modal-eliminar.html', context)
    else:
        permutas.delete()
        messages.success(request, 'Eliminación exitosa!')
        return redirect('permutas')            

def Filtrar_permutas(codigoempleado):
    if codigoempleado != '':
        permutas = Permuta.objects.filter(Q(personal__codigoempleado__icontains = codigoempleado))
    else:
        permutas = Permuta.objects.all().order_by('-id')
    
    return permutas 