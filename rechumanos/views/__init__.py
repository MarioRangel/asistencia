from .home import *
from .personales import*
from .viajes import*
from .faltas import*
from .checador import*
from .permisos import*
from .permutas import*
from .festivos import*
from .vacaciones import*
from .DF import*
from .mixto import*