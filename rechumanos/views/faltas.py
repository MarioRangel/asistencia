from django.forms.forms import Form
from django.shortcuts import get_object_or_404, render, redirect
from ..models import *
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse, Http404
from django.core import serializers
from rechumanos.forms import FaltaForm, FaltaFiltroForm
from django.db.models.query_utils import Q

# Create your views here.

@login_required
def faltas_all(request):
    if request.user.is_authenticated:
        faltas = Faltas.objects.all()
        context = { 'faltas': faltas, 'url_name': 'faltas'}
        return render(request, 'rechumanos/faltas/content.html', context)
    else:
        return redirect('login')

@login_required
def falta_nueva(request):
     if request.method == 'POST':
         current_user = get_object_or_404(User, pk=request.user.pk)
         form = FaltaForm(request.POST)
         if form.is_valid():
            faltas = form.save(commit=False)
            faltas.user = current_user
            faltas.orden = 1
            faltas.save()
            messages.success(request, 'Registro exitoso!')
            return redirect('faltas_filtro')
     else:
        form = FaltaForm()
     return render(request, 'rechumanos/faltas/modal-nuevo.html', {'form': form})

@login_required
def falta_editar(request,pk):
    faltas = get_object_or_404(Faltas,pk = pk)
    if request.method == 'POST':
        form = FaltaForm(request.POST, instance=faltas)
        if form.is_valid():
            form.save()
            messages.success(request, 'Edicion de falta exitosa!')
            return redirect('faltas_filtro')
    else:
        form = FaltaForm(instance=faltas)      
    return render(request, 'rechumanos/faltas/modal-editar.html', {'form': form, 'faltas': faltas})   

@login_required
def falta_eliminar(request, pk):
    falta = get_object_or_404(Faltas,pk = pk)       
    if request.is_ajax():
        context = { 'faltas': falta}
        return render(request, 'rechumanos/faltas/modal-eliminar.html', context)
    else:
        falta.delete()
        messages.success(request, 'Eliminación de falta exitosa!')
        return redirect('faltas_filtro')

@login_required
def falta_filtro(request):
    codigoempleado = ""
    if request.method == 'POST':
        form = FaltaFiltroForm(request.POST)
        if form.is_valid():
            codigoempleado = form.cleaned_data['codigoempleado']
    else:
        form = FaltaFiltroForm()

    falta = Filtrar_faltas(codigoempleado)
    context = {'form': form, 'faltas': falta, 'url_name': 'faltas_filtro'}
    return render(request, 'rechumanos/faltas/filtro.html', context)                 


def Filtrar_faltas(codigoempleado):
    if codigoempleado != '':
        falta = Faltas.objects.filter(Q(personal__codigoempleado__icontains = codigoempleado))
    else:
        falta = Faltas.objects.all().order_by('-id')

    return falta           