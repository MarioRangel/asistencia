from django.forms.forms import Form
from django.shortcuts import get_object_or_404, render, redirect
from ..models import *
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse, Http404
from django.core import serializers
from rechumanos.forms import VacacionesFiltroForm,VacacionesForm
from django.db.models.query_utils import Q

@login_required
def vacaciones_filtro(request):
    codigoempleado = ""
    if request.method == 'POST':
         form = VacacionesFiltroForm(request.POST)
         if form.is_valid():
             codigoempleado = form.cleaned_data['codigoempleado']
    else:
        form = VacacionesFiltroForm()

    vacaciones = Filtrar_vacaciones(codigoempleado)
    context = {'form': form, 'vacaciones': vacaciones, 'url_name': 'vacaciones_filtro'}
    return render(request, 'rechumanos/vacaciones/filtro.html', context) 

@login_required
def vacaciones_nuevo(request):
     if request.method == 'POST':
         current_user = get_object_or_404(User, pk=request.user.pk)
         form = VacacionesForm(request.POST)
         if form.is_valid():
            vacacion = form.save(commit=False)
            vacacion.user = current_user
            vacacion.orden = 1
            vacacion.save()
            messages.success(request, 'Registro exitoso!')
            return redirect('vacaciones')
     else:
        form = VacacionesForm()
     return render(request, 'rechumanos/vacaciones/modal-nuevo.html', {'form': form})     

@login_required
def vacaciones_editar(request,pk):
    vacaciones = get_object_or_404(Vacaciones,pk = pk)
    if request.method == 'POST':
        form = VacacionesForm(request.POST, instance= vacaciones)
        if form.is_valid():
            form.save()
            messages.success(request, 'Edicion exitosa!')
            return redirect('vacaciones')
    else:
        form = VacacionesForm(instance= vacaciones)      
    return render(request, 'rechumanos/vacaciones/modal-editar.html', {'form': form, 'Vacaciones':  vacaciones})

@login_required
def vacaciones_eliminar(request, pk):
    vacaciones = get_object_or_404(Vacaciones,pk = pk)       
    if request.is_ajax():
        context = { 'Vacaciones': vacaciones}
        return render(request, 'rechumanos/vacaciones/modal-eliminar.html', context)
    else:
        vacaciones.delete()
        messages.success(request, 'Eliminación exitosa!')
        return redirect('vacaciones')    

def Filtrar_vacaciones(codigoempleado):
    if codigoempleado != '':
        festivos = Vacaciones.objects.filter(Q(personal__codigoempleado__icontains = codigoempleado))
    else:
        festivos = Vacaciones.objects.all().order_by('-fechah_captura')[:50]
    
    return festivos       