from django.db import models

# Create your models here.
class Areas(models.Model):
    idarea = models.IntegerField(primary_key = True, default=0)
    area = models.CharField(max_length=255)

    class Meta:
       managed = False
       db_table = 'Areas'

    def __str__(self):
        return self.area

class Departamento(models.Model):
    idarea = models.ForeignKey(Areas,db_column='idarea', on_delete=models.CASCADE)
    iddepartamento = models.IntegerField(primary_key = True, default=0)
    departamento = models.CharField(max_length=255)

    class Meta:
        unique_together = (('iddepartamento', 'idarea'),)
        managed = False
        db_table = 'Departamento'

    def __str__(self):
        return self.departamento

class NivelPersonal(models.Model):
    idnivel = models.IntegerField(db_column='idnivel',primary_key = True, default=0)
    nivel = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'NivelPersonal'

    def __str__(self):
        return self.nivel

class horarios(models.Model):
    idhorarios = models.IntegerField(db_column='idhorarios',primary_key = True, default=0)
    descripcion = models.CharField(max_length=50)
    entrada_hora = models.DateTimeField()
    salida_hora = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'horarios'

    def __str__(self):
        return self.descripcion

class personal(models.Model):
    codigoempleado = models.IntegerField(db_column='codigoempleado',primary_key = True,default=0)
    nombre = models.CharField(max_length=50)
    paterno = models.CharField(max_length=50)
    materno = models.CharField(max_length=50)
    nombrelargo = models.CharField(max_length=255)
    puesto = models.CharField(max_length=255)
    idarea = models.ForeignKey(Areas, models.DO_NOTHING, db_column='idarea', blank=True, null=True)
    iddepartamento = models.ForeignKey(Departamento, models.DO_NOTHING, db_column='iddepartamento', blank=True, null=True)
    checa = models.CharField(max_length=50)
    estatus = models.CharField(max_length=10)
    baja = models.CharField(max_length=10)
    fecha_baja = models.DateTimeField(null=True, blank=True)
    fecha_ingreso = models.DateTimeField()
    fecha_nac = models.DateTimeField()
    idtipo_horario = models.ForeignKey(horarios, models.DO_NOTHING, db_column='idtipo_horario')
    nivel = models.ForeignKey(NivelPersonal, models.DO_NOTHING, db_column='nivel', blank=True, null=True)

    class Meta:
       managed = False
       db_table = 'personal'

    def __str__(self):
        return self.nombrelargo

    def nombre_completo(self):
        return '{} {} {}'.format(self.paterno, self.materno, self.nombre)


class lugar(models.Model):
    descripcion = models.CharField(max_length=50)

    def __str__(self):
        return self.descripcion

class viajes(models.Model):
    personal = models.ForeignKey(personal,on_delete=models.CASCADE)
    fecha_inicio = models.DateField()
    fecha_terminacion = models.DateField()
    lugar = models.ForeignKey(lugar,on_delete=models.CASCADE)
    otro = models.CharField(max_length=50, null=True, blank=True)
    observaciones = models.CharField(max_length=100,null=True, blank=True)

    def __str__(self):
        return self.observaciones

    def Codigo(self):
        return personal.codigoempleado   
     
class Faltas(models.Model):
    personal = models.ForeignKey(personal, on_delete=models.CASCADE)
    fecha = models.DateField()
    observaciones = models.CharField(max_length=100,null=True, blank=True)

    def __str__(self):
        return self.observaciones

class Checador(models.Model):
     personal = models.ForeignKey(personal, on_delete=models.CASCADE)
     fecha = models.DateField()
     hora_entrada = models.TimeField()
     hora_salida = models.TimeField()
     observaciones = models.CharField(max_length=100,null=True, blank=True)

     def __str__(self):
        return self.observaciones

class tipo_permisos(models.Model):
    descripcion = models.CharField(max_length=100)

    def __str__(self):
        return self.descripcion

class permisos(models.Model):
    personal = models.ForeignKey(personal, on_delete=models.CASCADE)
    fecha = models.DateField()
    tipo = models.ForeignKey(tipo_permisos, on_delete=models.CASCADE)
    hora = models.TimeField()
    hora_regreso = models.TimeField(null=True, blank=True)
    observaciones = models.CharField(max_length=100,null=True, blank=True)

    def __str__(self):
        return self.observaciones


class Permuta(models.Model):
    personal = models.ForeignKey(personal, on_delete=models.CASCADE)
    fecha = models.DateField()
    hora_entrada = models.TimeField(null=True, blank=True)
    hora_salida = models.TimeField(null=True, blank=True)
    otro = models.CharField(max_length=50, null=True, blank=True)
    fecha_descanso =  models.DateField(null=True, blank=True)
    observaciones = models.CharField(max_length=100,null=True, blank=True)

    def __str__(self):
        return self.observaciones

class Vi_vac_inc(models.Model):
    personal = models.ForeignKey(personal,models.DO_NOTHING, db_column='codigoempleado')
    fecha = models.DateTimeField(blank=True, null=True)
    concepto = models.CharField(max_length=50)
    motivo = models.CharField(max_length=4000,null=True, blank=True)
    otros = models.CharField(max_length=4000,null=True, blank=True)

    def __str__(self):
        return self.motivo

class Vacaciones(models.Model):
    idvacaciones = models.AutoField(primary_key=True)
    personal = models.ForeignKey(personal, models.DO_NOTHING, db_column='codigoempleado')
    fecha_inicio = models.DateTimeField()
    fecha_termino = models.DateTimeField()
    dias = models.IntegerField()
    fechah_captura = models.DateTimeField(auto_now_add=True)
    observaciones = models.CharField(max_length=255,null=True, blank=True)

    class Meta:
        managed = False
        db_table = 'Vacaciones'

    def __str__(self):
        return self.observaciones

class HorarioMixto(models.Model):
     personal = models.ForeignKey(personal, models.DO_NOTHING, db_column='codigoempleado')
     tipo = models.CharField(max_length=50,null=True, blank=True)
     hora_inicio = models.TimeField()
     hora_termina = models.TimeField()
     fecha = models.DateTimeField()
     observaciones = models.CharField(max_length=255,null=True, blank=True)

     def __str__(self):
        return self.observaciones


class Festivos(models.Model):
    idfestivos = models.AutoField(primary_key=True)
    fecha = models.DateTimeField()
    descripcion = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'festivos'













     
